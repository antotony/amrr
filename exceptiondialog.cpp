#include "exceptiondialog.h"
#include "ui_exceptiondialog.h"

ExceptionDialog::ExceptionDialog(QString message, QString action, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ExceptionDialog)
{
    ui->setupUi(this);
    ui->label->setText(message);
    this->setWindowFlag(Qt::WindowStaysOnTopHint);
    this->action = action;
}

ExceptionDialog::~ExceptionDialog()
{
    delete ui;
}

void ExceptionDialog::on_pushButton_clicked()
{
    if (this->action == "")
        QCoreApplication::quit();
    else
    {
        this->setHidden(true);
        system(this->action.toStdString().c_str());
        this->close();
    }
}
