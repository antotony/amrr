#include "repertorygrid.h"
#include "ui_repertorygrid.h"

RepertoryGrid::RepertoryGrid(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RepertoryGrid)
{
    ui->setupUi(this);
    this->setFixedSize(660, 485);
    int count_columns = 0;
    for (int i = 0; i < concepts.length(); ++i)
        count_columns += concepts[i].length();
    int count_rows = constructs.length();

    this->ui->right_construct->setRowCount(count_rows);
    this->ui->left_construct->setRowCount(count_rows);
    this->ui->repertory_grid->setRowCount(count_rows);
    this->ui->repertory_grid->setColumnCount(count_columns);
    this->ui->repertory_grid->setVerticalScrollBar(this->ui->left_construct->verticalScrollBar());
    this->ui->right_construct->setVerticalScrollBar(this->ui->repertory_grid->verticalScrollBar());
    QStringList hl;
    for (int i = 0; i < concepts.length(); ++i)
        for (int j = 0; j < concepts[i].length(); ++j)
            hl.append(concepts[i][j]);
    for (int i = 0; i < count_rows; ++i)
    {
        ui->left_construct->setItem(i, 0, new QTableWidgetItem(constructs[i].second, 0));
        ui->right_construct->setItem(i, 0, new QTableWidgetItem(constructs[i].first, 0));
    }
    this->ui->repertory_grid->setHorizontalHeaderLabels(hl);
    this->ui->repertory_grid->horizontalHeader()->setVisible(true);
    for (int i = 0; i < count_rows; ++i)
        for (int j = 0; j < count_columns; ++j)
            this->ui->repertory_grid->setItem(i, j, new QTableWidgetItem(QString(""), 0));
}

RepertoryGrid::~RepertoryGrid()
{
    delete ui;
}

void RepertoryGrid::on_OKButton_clicked()
{
    QFile result(path + "result.txt");
    result.open(QIODevice::WriteOnly);
    QList<QPair<QString, double>> mark_list;
    QList<QPair<QString, double>> mark_list2;
    QList<QPair<QString, double>> mark_list3;
    QList<QPair<QString, double>> mark_list4;
    for (int i = 0; i < ui->repertory_grid->columnCount(); ++i)
    {
        QString premise = ui->repertory_grid->horizontalHeaderItem(i)->text();
        QList<QString> cond;
        double mark = 0;
        double mark2 = 0;
        double mark3 = 0;
        double mark4 = 0;
        for (int j = 0; j < ui->repertory_grid->rowCount(); ++j)
        {
            QString text_value = ui->repertory_grid->item(j, i)->text();
            int item_value = ui->repertory_grid->item(j, i)->text().toInt();
            if (item_value == 0 && text_value != "0")
                item_value = -1;
            if (item_value >= 0 && item_value <= 100)
            {
                mark += abs(50 - item_value);
                mark2 += (abs(50 - item_value) >= 20?1:0);
                mark3 += (abs(50 - item_value) >= 20?1:-double(20 - abs(50 - item_value))/20);
                mark4 += (abs(50 - item_value) >= 20?double(abs(50 - item_value) - 20)/30:-double(20 - abs(50 - item_value))/20);
            }
        }
        mark /= ui->repertory_grid->rowCount();
        mark2 /= ui->repertory_grid->rowCount();
        mark3 /= ui->repertory_grid->rowCount();
        mark4 /= ui->repertory_grid->rowCount();
        mark_list.append(QPair<QString, double>(premise, mark));
        mark_list2.append(QPair<QString, double>(premise, mark2));
        mark_list3.append(QPair<QString, double>(premise, mark3));
        mark_list4.append(QPair<QString, double>(premise, mark4));
    }
    for (auto mark: mark_list4){
        result.write(((mark.first + ";").toStdString() + std::to_string(mark.second) + "\n").c_str());
    }
    result.close();
    ExceptionDialog *ed = new ExceptionDialog("Данные были успешно\nзаписаны.");
    ed->show();
    this->close();
}

void RepertoryGrid::on_cancelButton_clicked()
{
    this->close();
}
