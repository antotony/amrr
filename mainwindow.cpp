#include "mainwindow.h"
#include "ui_mainwindow.h"

QList<QList<QString>> concepts;

MainWindow::MainWindow(QString file_path, QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setFixedSize(570, 295);

    QFile input(path + file_path);
    logger.write("Чтение файла признаков из: " + (path + file_path).toStdString());
    if (!input.open(QIODevice::ReadOnly))
    {
        logger.write("Файл не найден");
        ExceptionDialog *mes = new ExceptionDialog("Файл ввода данных не найден.");
        mes->show();
        this->setEnabled(false);
    }
    input.close();
    if (true){
        DiffConcept *dc = new DiffConcept();
        dc->show();
        this->close();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_okButton_clicked()
{
    DiffConcept *dc = new DiffConcept();
    dc->show();
    this->close();
}



void MainWindow::on_checkBox_stateChanged(int arg1)
{
    if (arg1 == 2){
        logger.write("Включена опция 'Больше не показывать'");
    } else {
        logger.write("Отключена опция 'Больше не показывать'");
    }
}
