#include "diffconcepts.h"
#include "ui_diffconcepts.h"

QList<QPair<QString, QString>> constructs;

DiffConcept::DiffConcept(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DiffConcept)
{
    ui->setupUi(this);
    this->setFixedSize(370, 215);
    this->set_hidden_part2(true);
    set_triads();
    triads.open(QIODevice::ReadOnly);
    show_next_triad();
}

DiffConcept::~DiffConcept()
{
    delete ui;
}

void DiffConcept::set_hidden_part2(bool value)
{
    ui->label_5->setHidden(value);
    ui->label_6->setHidden(value);
    ui->pos_pole->setHidden(value);
    ui->neg_pole->setHidden(value);
    ui->continue1->setHidden(value);
}

void DiffConcept::set_triads()
{
    triads.setFileName(path + "triads.txt");
    triads.open(QIODevice::WriteOnly);
    int con_length = 0;
    for (int i = 0; i < concepts.length(); ++i)
        con_length += concepts[i].length();

    int concepts_count = 0;
    for (int i = 0; i < concepts.length(); ++i)
        concepts_count += concepts[i].length();

    if (concepts.length() > 1)
    for (int i = 1; i < concepts.length(); ++i) // для всех групп
    {
        for (int j = 0; j < concepts[i].length(); ++j) // для всех понятий групппы
        {
            for (int k = 0; k < concepts[i].length(); ++k)  // для всех понятий той же группы (выбирается второе понятие из группы)
            {
                if (k == j) // пропуск уже выбранного понятия
                    continue;
                for (int l = concepts.length() - 1; l >= 0; --l) // для всех групп
                {
                    if (l == i) // пропуск обрабатываемой группы
                        continue;
                    for (int o = 0; o < concepts[l].length(); ++o) // выбирается третье понятие
                    {
                        triads.write((concepts[i][j] + ";" + concepts[i][k] + ";" + concepts[l][o] + "\n").toStdString().c_str());
                    }
                }
            }
            triads.write("%;%;%\n");
        }
    }
    else
        if (concepts.length() > 0)
        {
            if (concepts[0].length() > 2)
            {
                for (int i = 0; i < concepts[0].length(); ++i)
                {
                    for (int j = 0; j < concepts[0].length(); ++j)
                    {
                        if (j == i)
                            continue;
                        for (int k = j + 1; k < concepts[0].length(); ++k)
                        {
                            if (k == i)
                                continue;
                            triads.write((concepts[0][i] + ";" + concepts[0][j] + ";" + concepts[0][k] + "\n").toStdString().c_str());
                        }
                    }
                    triads.write("%;%;%\n");
                }
            }
            else
            {
                ExceptionDialog *ed = new ExceptionDialog("Недостаточно симптомов\nдля выполнения модуля.");
                ed->show();
                this->setEnabled(false);
            }
        }
        else
        {
            ExceptionDialog *ed = new ExceptionDialog("Недостаточно симптомов\nдля выполнения модуля.");
            ed->show();
            this->setEnabled(false);
        }
    triads.close();
}

void DiffConcept::show_next_triad()
{
    char line[1024];
    if (!triads.atEnd())
    {
        triads.readLine(line, 1024);
        QString linestr(line);
        linestr.truncate(linestr.length() - 1);
        if (linestr != "%;%;%")
        {
            QStringList triad = linestr.split(";");
            ui->concept1->setText(triad[0]);
            ui->concept2->setText(triad[1]);
            ui->concept3->setText(triad[2]);
        }
        else
            if (triads.atEnd())
            {
                last_triad = true;
            }
            else
            {
                show_next_triad();
            }
    }
    else
        last_triad = true;
}

void DiffConcept::on_continue1_clicked()
{
    QPair<QString, QString> construct(ui->pos_pole->currentText(), ui->neg_pole->currentText());
    constructs.append(construct);
    if (last_triad)
    {
        RepertoryGrid* rg = new RepertoryGrid();
        rg->show();
        this->close();
    }
    else
    {
        this->setFixedSize(370, 215);
        ui->question->setText("Можно ли выделить критерий, по которому\nодин из симптомов отличается от двух других?");
        ui->yes_button->setHidden(false);
        ui->no_button->setHidden(false);
        ui->pos_pole->addItem(ui->pos_pole->currentText());
        ui->pos_pole->clearEditText();
        ui->pos_pole->setCurrentIndex(0);
        ui->neg_pole->addItem(ui->neg_pole->currentText());
        ui->neg_pole->clearEditText();
        ui->neg_pole->setCurrentIndex(0);
        this->set_hidden_part2(true);

        char line[1024];
        triads.readLine(line, 1024);
        QString linestr(line);
        while(linestr != "%;%;%\n")
        {
            triads.readLine(line, 1024);
            linestr = QString(line);
        }
        show_next_triad();
        if (last_triad)
        {
            RepertoryGrid* rg = new RepertoryGrid();
            rg->show();
            this->close();
        }
    }
}

void DiffConcept::on_yes_button_clicked()
{
    this->setFixedSize(370, 365);
    ui->question->setText("Введите критерий, по которому один\nиз симптомов отличается от двух других.");
    ui->yes_button->setHidden(true);
    ui->no_button->setHidden(true);
    this->set_hidden_part2(false);
    ui->label_6->setEnabled(false);
    ui->neg_pole->setEnabled(false);
    ui->continue1->setEnabled(false);
}

void DiffConcept::on_no_button_clicked()
{
    if (last_triad)
    {
        RepertoryGrid* rg = new RepertoryGrid();
        rg->show();
        this->close();
    }
    else
    {
        show_next_triad();
    }
}

void DiffConcept::on_pushButton_clicked()
{
    this->setFixedSize(370, 215);
    ui->question->setText("Можно ли выделить критерий, по которому\nодин из симптомов отличается от двух других?");
    ui->yes_button->setHidden(false);
    ui->no_button->setHidden(false);
    ui->pos_pole->clearEditText();
    ui->neg_pole->clearEditText();
    this->set_hidden_part2(true);
}

void DiffConcept::on_pos_pole_currentIndexChanged(int index)
{
    ui->neg_pole->setCurrentIndex(index);
}

void DiffConcept::on_neg_pole_currentIndexChanged(int index)
{
    ui->pos_pole->setCurrentIndex(index);
}

void DiffConcept::on_pos_pole_editTextChanged(const QString &arg1)
{
    ui->label_6->setEnabled(true);
    ui->neg_pole->setEnabled(true);
}

void DiffConcept::on_neg_pole_editTextChanged(const QString &arg1)
{
    ui->continue1->setEnabled(true);
}
