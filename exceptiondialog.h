#ifndef EXCEPTIONDIALOG_H
#define EXCEPTIONDIALOG_H

#include <QDialog>

namespace Ui {
class ExceptionDialog;
}

class ExceptionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ExceptionDialog(QString message, QString action = "", QWidget *parent = nullptr);
    ~ExceptionDialog();

private slots:
    void on_pushButton_clicked();

private:
    Ui::ExceptionDialog *ui;

    QString action;
};

#endif // EXCEPTIONDIALOG_H
