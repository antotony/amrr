#include "mainwindow.h"
#include <QApplication>
#include<iostream>
#include"logger.h"

Logger logger;
QString path;

QString get_path()
{
    QDir dir;
    QString path = dir.currentPath();
    path.truncate(dir.currentPath().lastIndexOf('/') + 1);
    return path;
}

int main(int argc, char *argv[])
{
    int result;
    path = get_path();
    logger.setFileName(path + "logs.txt");
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
        result = a.exec();
    return result;
}
