#ifndef DIFFCONCEPT_H
#define DIFFCONCEPT_H

#include <QDialog>
#include"repertorygrid.h"
#include"exceptiondialog.h"
#include<fstream>
#include"logger.h"

extern Logger logger;
extern QString path;
extern QList<QList<QString>> concepts;
extern QList<QPair<QString, QString>> constructs;

namespace Ui {
class DiffConcept;
}

class DiffConcept : public QDialog
{
    Q_OBJECT

public:
    explicit DiffConcept(QWidget *parent = nullptr);
    ~DiffConcept();

private slots:

    void on_continue1_clicked();

    void on_yes_button_clicked();

    void on_no_button_clicked();

    void on_pushButton_clicked();

    void on_pos_pole_currentIndexChanged(int index);

    void on_neg_pole_currentIndexChanged(int index);

    void on_pos_pole_editTextChanged(const QString &arg1);

    void on_neg_pole_editTextChanged(const QString &arg1);

private:
    bool last_triad = false;
    Ui::DiffConcept *ui;
    QFile triads;

    void set_hidden_part2(bool);
    void set_triads();
    void show_next_triad();
};

#endif // DIFFCONCEPT_H
