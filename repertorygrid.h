#ifndef REPERTORYGRID_H
#define REPERTORYGRID_H

#include <QDialog>
#include<QFile>
#include"exceptiondialog.h"
#include<cstdlib>
#include<math.h>
#include"logger.h"

extern Logger logger;
extern QString path;
extern QList<QList<QString>> concepts;
extern QList<QPair<QString, QString>> constructs;

namespace Ui {
class RepertoryGrid;
}

class RepertoryGrid : public QDialog
{
    Q_OBJECT

public:
    explicit RepertoryGrid(QWidget *parent = nullptr);
    ~RepertoryGrid();

private slots:
    void on_OKButton_clicked();

    void on_cancelButton_clicked();

private:
    Ui::RepertoryGrid *ui;
};

#endif // REPERTORYGRID_H
