#ifndef LOGGER_H
#define LOGGER_H

#include<QFile>
#include<iostream>

class Logger
{
public:
    Logger()
    {
        logger_file = new QFile();
    }

    Logger(const char* path)
    {
        logger_file = new QFile(path);
    }

    ~Logger()
    {
        free(logger_file);
        logger_file = nullptr;
    }

    void write(const std::string line = "")
    {
        std::cout << line + '\n';
        if (logger_file->open(QIODevice::WriteOnly | QIODevice::Append))
        {
            logger_file->write((line + '\n').c_str());
            logger_file->close();
        }
    }

    void setFileName(const QString& path)
    {
        logger_file->setFileName(path);
        this->write(("Перезаписан файл логов: " + path).toStdString());
    }
private:
    QFile *logger_file;
};

#endif // LOGGER_H
