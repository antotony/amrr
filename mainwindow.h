#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include<QDir>
#include<QFile>
#include"diffconcepts.h"
#include"logger.h"
#include"exceptiondialog.h"

extern Logger logger;
extern QString path;
extern QList<QList<QString>> concepts;

QString get_path();

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QString file_path = "input.txt", QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_okButton_clicked();

    void on_checkBox_stateChanged(int arg1);

private:
    Ui::MainWindow *ui;
    int _old_width = 260;
    struct MWExceptions{
        struct NoInputFile{};
    };
};
#endif // MAINWINDOW_H
